#!/bin/bash

OUTPUT_DIR=$1

if [ ! -d ${OUTPUT_DIR} ];
then
  mkdir ${OUTPUT_DIR}
fi

while IFS=, read -r SAMPLE UNUSED
do
    echo ">>> Download sample ${SAMPLE}"
    xrdcp root://eospublic.cern.ch//eos/root-eos/HiggsTauTauReduced/${SAMPLE}.root ${OUTPUT_DIR}
done < skim.csv
